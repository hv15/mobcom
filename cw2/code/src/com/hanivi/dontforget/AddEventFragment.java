package com.hanivi.dontforget;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

public class AddEventFragment extends DialogFragment
{
	private static final String	TAG	= "AddEventFragment";
	private String ftitle;
	private String fdescription;
	private String flocation;
	private Date fstart;
	private Date fend;
	private boolean fday;
	private boolean frecur;
	private String fduration;
	private String frule;
	private int fcalendar;
	
	public interface AddEventFragmentListener {
        void onReturnAddEventFragment(Uri uri);
    }
	
	public static AddEventFragment newInstance(int calendarID)
	{
		AddEventFragment frag = new AddEventFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("calendarid", calendarID);
		frag.setArguments(bundle);
		return frag;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Bundle bundle = getArguments();
		if(bundle == null || bundle.isEmpty()){
			Log.e(TAG, "Bundle is empty, we need to fail now!");
			fcalendar = -1;
		} else {
			fcalendar = bundle.getInt("calenderid");
		}
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.add_event_dialog, null);
		
		final EditText title = (EditText) view.findViewById(R.id.title_add_event);
		final EditText description = (EditText) view.findViewById(R.id.description_add_event);
		final EditText location = (EditText) view.findViewById(R.id.location_add_event);
		
		final EditText stime = (EditText) view.findViewById(R.id.stime_add_event);
		final EditText sdate = (EditText) view.findViewById(R.id.sdate_add_event);
		final EditText etime = (EditText) view.findViewById(R.id.etime_add_event);
		final EditText edate = (EditText) view.findViewById(R.id.edate_add_event);
		
		final CheckBox day = (CheckBox) view.findViewById(R.id.day_checkbox_add_event);
		
		final CheckBox recur = (CheckBox) view.findViewById(R.id.repeat_checkbox_add_event);
		final EditText number = (EditText) view.findViewById(R.id.repeat_value_add_event);
		final Spinner repeat = (Spinner) view.findViewById(R.id.repeat_spinner_add_event);
		
		stime.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					timeDialog(stime);
				}
				return true;
			}
		});
		
		sdate.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					dateDialog(sdate);
				}
				return true;
			}
		});
		
		etime.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					timeDialog(etime);
				}
				return true;
			}
		});
		
		edate.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					dateDialog(edate);
				}
				return true;
			}
		});
		
		AlertDialog.Builder dialog = new Builder(getActivity())
		.setTitle("Add Event")
		.setPositiveButton("Add", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if(fcalendar == -1){
					Log.e(TAG, "Tried to add event with invalid calendar ID");
					dialog.dismiss();
				}
				Uri eventid = null;
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.US);
				ftitle = title.getText().toString();
				fdescription = description.getText().toString();
				flocation = location.getText().toString();
				String ssmil = stime.getText().toString() + " " + sdate.getText().toString();
				String esmil = etime.getText().toString() + " " + edate.getText().toString();
				try{
					fstart = sdf.parse(ssmil);
					fend = sdf.parse(esmil);
				} catch (ParseException e){
					e.printStackTrace();
				}
				Log.v(TAG, "Start time: " + ssmil + " => " + fstart.getTime());
				Log.v(TAG, "End time: " + esmil + " => " + fend.getTime());
				fday = day.isChecked();
				frecur = recur.isChecked();
				
				Log.i(TAG, "About to add new event");
				if(frecur){
					fduration = Provider.getDuration(fstart.getTime(), fend.getTime());
					frule = "FREQ=" + Provider.RULE_FREQ[repeat.getSelectedItemPosition()] + ";COUNT=" + number.getText().toString().trim();
					eventid = Provider.addRecuEvent(ftitle, fdescription, fcalendar, fstart.getTime(), fend.getTime(), fday, fduration, frule, getActivity().getContentResolver());
				} else {
					eventid = Provider.addEvent(ftitle, fdescription, fcalendar, fstart.getTime(), fend.getTime(), fday, getActivity().getContentResolver());
				}
				((EventListFragment) getParentFragment()).onReturnAddEventFragment(eventid);
				dialog.dismiss();
			}
		})
		.setNegativeButton("Cancel", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		
		dialog.setView(view);
		return dialog.create();
	}
	
	private void timeDialog(final EditText area)
	{
		Calendar time = Calendar.getInstance();
		int hour = time.get(Calendar.HOUR_OF_DAY);
		int minute = time.get(Calendar.MINUTE);
		TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute)
			{
				area.setText(hourOfDay + ":" + minute);
			}
		}, hour, minute, true);
		tpd.setTitle("Select Time");
		tpd.show();
	}
	
	private void dateDialog(final EditText area)
	{
		Calendar time = Calendar.getInstance();
		int day = time.get(Calendar.DAY_OF_MONTH);
		int month = time.get(Calendar.MONTH);
		int year = time.get(Calendar.YEAR);
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet (DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				area.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
			}
		}, year, month, day);
		dpd.setTitle("Select Date");
		dpd.show();
	}
}
