package com.hanivi.dontforget;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class FindEventFragment extends DialogFragment
{
	public interface FindEventFragmentListener {
        void onReturnFindEventFragment();
    }

	protected static final String	TAG	= "FindEventFragment";
	
	public static FindEventFragment newInstance()
	{
		FindEventFragment frag = new FindEventFragment();
		return frag;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.find_event_dialog, null);
		
		final EditText id = (EditText) view.findViewById(R.id.find_event_id);
		
		AlertDialog.Builder dialog = new Builder(getActivity())
		.setTitle("Find Event")
		.setPositiveButton("Find", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Cursor cur = Provider.getEvent(id.getText().toString(), getActivity().getContentResolver());
				ArrayList<String[]> list = Provider.getList(cur, Provider.EVENT);
				if(list.isEmpty() || list.get(0) == null){
					Log.e(TAG, "Event " + id.getText().toString() + " could not be found");
				} else {
					Log.i(TAG, "Printing Event information");
					for(String[] item : list){
						Log.i(TAG, "Found " + item[0] + ": " + item[1] + " in calendar " + item[5] + ". Starting at " + item[2]);
					}
				}
				dialog.dismiss();
			}
		})
		.setNegativeButton("Cancel", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		
		dialog.setView(view);
		return dialog.create();
	}
}