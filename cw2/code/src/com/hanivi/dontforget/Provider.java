package com.hanivi.dontforget;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.util.Log;

public class Provider
{
	private static final String		TAG								= "Provider";
	public static final String[]	CALENDAR_PROJECTION				= new String[] {
			Calendars._ID, Calendars.ACCOUNT_NAME,
			Calendars.CALENDAR_DISPLAY_NAME, Calendars.OWNER_ACCOUNT };
	public static final String[]	EVENT_PROJECTION				= new String[] {
			Events._ID, Events.TITLE, Events.DTSTART, Events.DTEND,
			Events.ALL_DAY, Events.CALENDAR_ID											};
	public static final String[]	RULE_FREQ						= new String[] {
			"DAILY", "WEEKLY", "MONTHLY", "YEARLY"					};

	public static final int			CALENDAR						= 0;
	public static final int			EVENT							= 1;
	private static final int		CALENDAR_ID_INDEX				= 0;
	private static final int		CALENDAR_ACCOUNT_NAME_INDEX		= 1;
	private static final int		CALENDAR_DISPLAY_NAME_INDEX		= 2;
	private static final int		CALENDAR_OWNER_ACCOUNT_INDEX	= 3;
	private static final int		EVENT_ID_INDEX					= 0;
	private static final int		EVENT_TITLE_INDEX				= 1;
	private static final int		EVENT_START_INDEX				= 2;
	private static final int		EVENT_END_INDEX					= 3;
	private static final int		EVENT_DAY_FLAG_INDEX			= 4;
	private static final int		EVENT_CALENDAR_ID				= 5;

	public static Cursor getCalendars(String account, String accounttype,
			ContentResolver cr)
	{
		Cursor cur = null;
		Uri uri = Calendars.CONTENT_URI;

		String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND ("
				+ Calendars.ACCOUNT_TYPE + " = ?))";
		String[] selectionArgs = new String[] { account, accounttype };

		cur = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

		return cur;
	}

	public static Cursor getEvents(String calendarID, ContentResolver cr)
	{
		Cursor cur = null;
		Uri uri = Events.CONTENT_URI;
		Long time = Calendar.getInstance().getTimeInMillis();

		String selection = "((" + Events.CALENDAR_ID + " = ?) AND ("
				+ Events.DTSTART + " >= ?))";
		String[] selectionArgs = new String[] { calendarID, time.toString() };

		cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);

		return cur;
	}
	
	public static Cursor getEvent(String eventID, ContentResolver cr)
	{
		Cursor cur = null;
		Uri uri = Events.CONTENT_URI;
		
		String selection = "((" + Events._ID + " = ?))";
		String[] selectionArgs = new String[]{eventID};
		
		cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);
		
		return cur;
	}

	public static Uri addEvent(String title, String description,
			int calendarID, long dtstart, long dtend, boolean day,
			ContentResolver cr)
	{
		Log.i(TAG, "Adding standard event");
		ContentValues cv = new ContentValues();
		cv.put(Events.DTSTART, dtstart);
		cv.put(Events.DTEND, dtend);
		cv.put(Events.ALL_DAY, day);
		cv.put(Events.TITLE, title);
		cv.put(Events.DESCRIPTION, description);
		cv.put(Events.CALENDAR_ID, calendarID);
		cv.put(Events.EVENT_TIMEZONE,
				TimeZone.getDefault().getDisplayName(Locale.US));

		Uri uri = cr.insert(Events.CONTENT_URI, cv);
		Log.i(TAG, "event id is " + uri);
		return uri;
	}

	public static Uri addRecuEvent(String title, String description,
			int calendarID, long dtstart, long dtend, boolean day,
			String duraton, String rrule, ContentResolver cr)
	{
		Log.i(TAG, "Adding recurring event");
		ContentValues cv = new ContentValues();
		cv.put(Events.DTSTART, dtstart);
		cv.put(Events.DTEND, dtend);
		cv.put(Events.ALL_DAY, day);
		cv.put(Events.DURATION, duraton);
		cv.put(Events.RRULE, rrule);
		cv.put(Events.TITLE, title);
		cv.put(Events.DESCRIPTION, description);
		cv.put(Events.CALENDAR_ID, calendarID);
		cv.put(Events.EVENT_TIMEZONE,
				TimeZone.getDefault().getDisplayName(Locale.US));

		Uri uri = cr.insert(Events.CONTENT_URI, cv);
		Log.i(TAG, "event id is " + uri);
		return uri;
	}

	public static ArrayList<String[]> getList(Cursor cue, int type)
	{
		ArrayList<String[]> array = new ArrayList<String[]>(cue.getCount());
		switch (type) {
		case CALENDAR:
			while (cue.moveToNext())
			{
				array.add(new String[] { cue.getString(CALENDAR_ID_INDEX),
						cue.getString(CALENDAR_ACCOUNT_NAME_INDEX),
						cue.getString(CALENDAR_DISPLAY_NAME_INDEX),
						cue.getString(CALENDAR_OWNER_ACCOUNT_INDEX) });
			}
			break;
		case EVENT:
			while (cue.moveToNext())
			{
				array.add(new String[] { cue.getString(EVENT_ID_INDEX),
						cue.getString(EVENT_TITLE_INDEX),
						cue.getString(EVENT_START_INDEX),
						cue.getString(EVENT_END_INDEX),
						cue.getString(EVENT_DAY_FLAG_INDEX),
						cue.getString(EVENT_CALENDAR_ID)});
			}
			break;
		default:
			Log.e(TAG, "Invalid type was tried, returning empty ArrayList");
		}

		return array;
	}

	public static String getDuration(long start, long end)
	{
		long dur = end - start;
		SimpleDateFormat sdf = new SimpleDateFormat("'P'w'W'd'D'k'H'm'M's'S'",
				Locale.US);
		return sdf.format(new Date(dur));
	}
}
