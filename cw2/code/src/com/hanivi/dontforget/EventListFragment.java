package com.hanivi.dontforget;

import java.util.ArrayList;

import com.hanivi.dontforget.AddEventFragment.AddEventFragmentListener;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class EventListFragment extends ListFragment implements AddEventFragmentListener
{
	private static final String	TAG	= "EventListFragment";
	private String calendarID;
	private String calendarName;
	private ArrayList<String[]> eventsArray;
	
	public static EventListFragment newInstance(String calendarID, String calendarName)
	{
		EventListFragment frag = new EventListFragment();
		Bundle bundle = new Bundle();
		bundle.putString("calendarid", calendarID);
		bundle.putString("calendarname", calendarName);
		frag.setArguments(bundle);
		return frag;
	}
	
	private ArrayList<String[]> getEvents(String calendarID)
	{
		Cursor cue = Provider.getEvents(calendarID, getActivity().getContentResolver());
		return Provider.getList(cue, Provider.EVENT);
	}
	
	private void addEventDialog()
	{
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		Fragment frag = getChildFragmentManager().findFragmentByTag("adddialog");
		if(frag != null){
			ft.remove(frag);
		}
		ft.addToBackStack(null);
		
		AddEventFragment dial = AddEventFragment.newInstance(Integer.parseInt(this.calendarID));
		dial.show(ft, "adddialog");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		this.calendarID = getArguments().getString("calendarid");
		this.calendarName = getArguments().getString("calendarname");
		
		Log.i(TAG, "Opening events list for calendar " + this.calendarID);
		
		this.eventsArray = getEvents(this.calendarID);
		
		EventAdaptor adapter = new EventAdaptor(inflater.getContext(), R.layout.event_item, this.eventsArray);
		getActivity().setTitle(this.calendarName);
		setListAdapter(adapter);
		
		setHasOptionsMenu(true);

		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.event_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId()){
		case R.id.add_event:
			addEventDialog();
			return true;
		default:
            return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onReturnAddEventFragment(Uri uri)
	{
		Log.i(TAG, "Event has been added; refreshing list");
		((EventAdaptor) getListAdapter()).notifyDataSetChanged();
	}
}