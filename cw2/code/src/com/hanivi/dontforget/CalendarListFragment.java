package com.hanivi.dontforget;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class CalendarListFragment extends ListFragment
{
	private static final String	TAG	= "CalendarListFragment";
	private String accountName;
	private String accountType;
	private ArrayList<String[]> calendarsArray;
	
	public static CalendarListFragment newInstance(String accountName, String accountType)
	{
		CalendarListFragment frag = new CalendarListFragment();
		Bundle bundle = new Bundle();
		bundle.putString("account", accountName);
		bundle.putString("type", accountType);
		frag.setArguments(bundle);
		return frag;
	}
	
	private ArrayList<String[]> getCalendars(String accountName, String accountType)
	{
		Cursor cue = Provider.getCalendars(accountName, accountType, getActivity().getContentResolver());
		return Provider.getList(cue, Provider.CALENDAR);
	}
	
	private String[] getArray(ArrayList<String[]> array)
	{
		String[] list = new String[array.size()];
		for (int i = 0; i < array.size(); i++)
		{
			list[i] = array.get(i)[2];
		}
		return list;
	}
	
	private void findEventDialog()
	{
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		Fragment frag = getChildFragmentManager().findFragmentByTag("finddialog");
		if(frag != null){
			ft.remove(frag);
		}
		ft.addToBackStack(null);
		
		FindEventFragment dial = FindEventFragment.newInstance();
		dial.show(ft, "finddialog");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		this.accountName = getArguments().getString("account");
		this.accountType = getArguments().getString("type");
		
		this.calendarsArray = getCalendars(this.accountName, this.accountType);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				inflater.getContext(), android.R.layout.simple_list_item_1,
				getArray(this.calendarsArray));
		getActivity().setTitle(this.accountName + "'s Calendars");
		setListAdapter(adapter);
		
		setHasOptionsMenu(true);

		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		Log.i(TAG, "Clicked on " + this.calendarsArray.get(position)[1]);
		EventListFragment frag = EventListFragment.newInstance(this.calendarsArray.get(position)[0], this.calendarsArray.get(position)[2]);
		openEvent(frag, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.calendar_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId()){
		case R.id.find_event:
			findEventDialog();
			return true;
		default:
            return super.onOptionsItemSelected(item);
		}
	}
	
	private void openEvent(Fragment frag, int transit)
	{
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(android.R.id.content, frag);
		ft.setTransition(transit);
		ft.addToBackStack(null);
		ft.commit();
	}
}
