package com.hanivi.dontforget;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EventAdaptor extends ArrayAdapter<String[]>
{

	private static final String	TAG	= "EventAdaptor";

	public EventAdaptor(Context context, int resource,
			List<String[]> objects)
	{
		super(context, resource, objects);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		if(view == null){
			LayoutInflater inflator = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflator.inflate(R.layout.event_item, null);
		}
		
		String[] item = getItem(position);
		if(item != null){
			TextView title = (TextView) view.findViewById(R.id.title_event_item);
			if(title != null){
				title.setText(item[1]);
			}
			TextView startend = (TextView) view.findViewById(R.id.start_end_event_item);
			if(startend != null){
				DateFormat norm = DateFormat.getDateTimeInstance();
				DateFormat day = DateFormat.getDateInstance();
				Log.d(TAG, "setting time " + item[2] + " - " + item[3] + " DAY: " + item[4]);
				if(item[4].equals("1")){
					startend.setText(String.format("%s DAY EVENT", day.format(new Date(Long.parseLong(item[2])))));
				} else {
					if(item[3] == null){
						startend.setText(String.format("%s EXTENDED", norm.format(new Date(Long.parseLong(item[2])))));
					} else {
						startend.setText(String.format("%s to %s", norm.format(new Date(Long.parseLong(item[2]))), norm.format(new Date(Long.parseLong(item[3])))));
					}
				}
			}
		}
		
		return view;
	}
}
