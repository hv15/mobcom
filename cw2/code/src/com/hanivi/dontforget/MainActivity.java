package com.hanivi.dontforget;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity
{
	private static final int	ACCOUNT_CHOOSE	= 0;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		boolean accountSet = savedInstanceState != null ? savedInstanceState.getBoolean("accountset") : false;

		if(!accountSet){
			Intent intent = AccountManager.newChooseAccountIntent(null, null,
					new String[] { "com.google" }, true, null, null, null, null);
			startActivityForResult(intent, ACCOUNT_CHOOSE);
		}
	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data)
	{
		if (requestCode == ACCOUNT_CHOOSE && resultCode == RESULT_OK)
		{
			String accountName = data
					.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			String accountType = data
					.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
			FragmentManager fm = this.getFragmentManager();
			if (fm.findFragmentById(android.R.id.content) == null)
			{
				CalendarListFragment frag = CalendarListFragment.newInstance(accountName,
						accountType);
				fm.beginTransaction().add(android.R.id.content, frag).commit();
			}
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putBoolean("accountset", true);
	}
}