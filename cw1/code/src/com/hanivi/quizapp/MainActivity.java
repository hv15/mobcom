package com.hanivi.quizapp;

import java.util.ArrayList;
import java.util.Locale;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends FragmentActivity
{
	public static final String TAG = "MainActivity";
	private SectionsPagerAdaptor mSectionPagerAdaptor;
	private ViewPager mViewPager;
	private TextView mScore;
	private int score;
	private ArrayList<Category> categories;
	private int question_size;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Log.i(TAG, "Starting XML Parsing");
		QuestionFactory fc = new QuestionFactory(getAssets());
		this.categories = fc.readXML("questions.xml");
		this.question_size = fc.getQuestionSize();
		
		Log.i(TAG, "Finished parsing, creating Fragment Views");
		this.mSectionPagerAdaptor = new SectionsPagerAdaptor(getSupportFragmentManager());
		
		final ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);

		Log.i(TAG, "Setting up Scoring");
		this.mScore = (TextView) findViewById(R.id.score);
		this.mViewPager = (ViewPager) findViewById(R.id.pager);
		this.score = 0;
		setScore();
		
		Log.i(TAG, "Main process beginning");
		mViewPager.setAdapter(mSectionPagerAdaptor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		return super.onCreateOptionsMenu(menu);
	}
	
	private void setScore()
	{
		Locale l = Locale.getDefault();
		mScore.setText(("score " + this.score + " out of " + this.question_size).toUpperCase(l));
	}
	
	public void incScore()
	{
		this.score++;
		setScore();
	}

	public class SectionsPagerAdaptor extends FragmentPagerAdapter
	{
		public SectionsPagerAdaptor(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int postion)
		{
			Log.i(TAG, "Getting Category at index " + postion);
			return categories.get(postion);
		}

		@Override
		public int getCount() {
			return categories.size();
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			Log.i(TAG, "Getting Category Title at index " + position);
			Locale l = Locale.getDefault();
			return categories.get(position).name.toUpperCase(l);
		}
	}
}