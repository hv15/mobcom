package com.hanivi.quizapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class CategoryCompleteAlert extends DialogFragment
{
	public static CategoryCompleteAlert newInstance(String category)
	{
		CategoryCompleteAlert d = new CategoryCompleteAlert();
		Bundle args = new Bundle();
		args.putString("category", category);
		d.setArguments(args);
		return d;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		String category = getArguments().getString("category");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("You have completed " + category + "!")
               .setPositiveButton(R.string.awesome, null);
        return builder.create();
    }
}
