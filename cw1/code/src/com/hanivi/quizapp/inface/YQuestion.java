package com.hanivi.quizapp.inface;

public interface YQuestion
{
	public static final String TAG = "YQuestion";
	public String[] answer = {"yes", "no"};

	public String getQuestion();

	public String getAnswer();

	public boolean isCorrect(String a);
	
	public boolean isCorrect(int o);

}