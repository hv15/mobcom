package com.hanivi.quizapp.inface;

import java.util.ArrayList;

public interface MQuestion
{
	public static final String TAG = "MCQuestion";
	public ArrayList<String> options = new ArrayList<String>(4);
	public int correctAnswer = -1;

	public void addOption(String s, boolean correct);
	
	public void addOption(String a);

	public String getQuestion();

	public String getAnswer(int i);

	public String[] getOptions();

	public boolean isCorrect(int i);
}