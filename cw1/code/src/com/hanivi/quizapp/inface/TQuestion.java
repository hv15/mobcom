package com.hanivi.quizapp.inface;

public interface TQuestion
{
	public static final String TAG = "TQuestion";

	public String getQuestion();

	public String getAnswer();

	public boolean isCorrect(String a);
}