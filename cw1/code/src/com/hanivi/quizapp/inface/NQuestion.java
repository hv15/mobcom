package com.hanivi.quizapp.inface;

public interface NQuestion
{
	public static final String TAG = "NQuestion";
	
	public void setQuestion(String q);
	
	public String evalEquation(String[] q);

	public String getQuestion();

	public String getAnswer();

	public boolean isCorrect(String a);
}