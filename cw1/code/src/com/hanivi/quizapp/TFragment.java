package com.hanivi.quizapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TFragment extends Fragment {
	
	private EditText answer;
	private TextView question;
	private Button button;
	private Question q;

	public static TFragment newInstance(Question question)
	{
		TFragment frag = new TFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("question", question);
		frag.setArguments(bundle);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.text, container, false);
		this.question = (TextView) view.findViewById(R.id.text_question);
		this.answer = (EditText) view.findViewById(R.id.text_answer);
		this.button = (Button) view.findViewById(R.id.text_answer_btn);
		this.q = (Question) getArguments().getSerializable("question");
		
		this.question.setText(this.q.getQuestion());
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		this.button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(answer.getText().toString().equalsIgnoreCase(q.getAnswer())){
					((MainActivity) getActivity()).incScore();
				}
				((Category) getParentFragment()).replaceFrag();
			}
		});
	}
}
