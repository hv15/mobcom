package com.hanivi.quizapp;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Category extends Fragment
{
	public final static String TAG = "Category";
	public String name;

	private Question q;
	private int number = 0; // This is on purpose
	private ArrayList<Question> questions = new ArrayList<Question>();
	private Bundle savedState = null;

	public static Category newInstance(String name)
	{
		Log.d(TAG, "Creating a newInstance");
		Category frag = new Category();
		frag.name = name;
		return frag;
	}

	public void addQuestion(Question question)
	{
		this.questions.add(question);
		Collections.shuffle(this.questions);
	}

	public Question[] getQuestions()
	{
		return this.questions.toArray(new Question[1]);
	}

	public int size()
	{
		return this.questions.size();
	}

	private Fragment getFragType(String type, Question q)
	{
		if(type.equalsIgnoreCase("y")){
			return YFragment.newInstance(q);
		} else if(type.equalsIgnoreCase("m")){
			return MFragment.newInstance(q);
		} else if(type.equalsIgnoreCase("t")){
			return TFragment.newInstance(q);
		} else if(type.equalsIgnoreCase("n")){
			return NFragment.newInstance(q);
		} else {
			// We should never get here
			return DoneFragment.newInstance(this.name);
		}
	}

	public void replaceFrag()
	{
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		if(this.number < size()){
			Log.d(TAG,"For " + this.name + ", replacing at index " + this.number);
			this.q = this.questions.get(this.number);
			this.number += 1;
			ft.replace(R.id.fragment_main, getFragType(this.q.getType(), this.q));
		} else {
			ft.replace(R.id.fragment_main, DoneFragment.newInstance(this.name));
			this.number = size() + 1; // small hack to ensure that the instance is correctly restored
		}
		ft.addToBackStack(null)
		.commit();
	}

	public void replaceFrag(Question q)
	{
		getChildFragmentManager().beginTransaction()
		.replace(R.id.fragment_main, getFragType(q.getType(), q))
		.addToBackStack(null)
		.commit();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// Retain this fragment across configuration changes.
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedState == null){
			this.savedState = savedInstanceState.getBundle("state");
		}

		if(savedState != null){
			Log.d(TAG, "Getting Saved instance");
			this.number = savedState.getInt("number");
			this.savedState = null;
		} else {
			Log.d(TAG, "No Saved Instance");
			this.number = 0;
		}

		return inflater.inflate(R.layout.activity_main_fragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		replaceFrag();
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		this.savedState = savedState();
		this.number = -1;
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		/* If onDestroyView() is called first, we can use the previously savedState but we can't call saveState() anymore */
		/* If onSaveInstanceState() is called first, we don't have savedState, so we need to call saveState() */
		/* => (?:) operator inevitable! */
		savedInstanceState.putBundle("state", savedState != null ? savedState : savedState());
	}

	private Bundle savedState()
	{
		Log.d(TAG, "Saving Saved instance");
		Bundle state = new Bundle();
		state.putInt("number", this.number - 1);
		return state;
	}
}