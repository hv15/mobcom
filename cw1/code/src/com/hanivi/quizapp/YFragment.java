package com.hanivi.quizapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class YFragment extends Fragment {
	
	private TextView question;
	private Button yes, no;
	private Question q;

	public static YFragment newInstance(Question question)
	{
		YFragment frag = new YFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("question", question);
		frag.setArguments(bundle);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.yesno, container, false);
		this.question = (TextView) view.findViewById(R.id.yesno_question);
		this.yes = (Button) view.findViewById(R.id.yesno_yes_btn);
		this.no = (Button) view.findViewById(R.id.yesno_no_btn);
		this.q = (Question) getArguments().getSerializable("question");
		
		this.question.setText(this.q.getQuestion());
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		OnClickListener test = new OnClickListener() {
			
			Button btn;
			@Override
			public void onClick(View v) {
				switch(v.getId()){
				case R.id.yesno_no_btn:
					btn = no;
					break;
				case R.id.yesno_yes_btn:
					btn = yes;
					break;
				}
				if(btn.getText().toString().equalsIgnoreCase(q.getAnswer())){
					((MainActivity) getActivity()).incScore();
				}
				((Category) getParentFragment()).replaceFrag();
			}
		};
		this.yes.setOnClickListener(test);
		this.no.setOnClickListener(test);
	}
}