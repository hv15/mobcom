package com.hanivi.quizapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.res.AssetManager;
import android.util.Log;

public class QuestionFactory
{
	public static final String TAG = "QuestionFactory";
	private AssetManager assets;
	private int question_size;
	
	public QuestionFactory(AssetManager assets)
	{
		Log.d(TAG, "Instanziated the QuestionFactory");
		this.assets = assets;
		this.question_size = 0;
	}
	
	public ArrayList<Category> readXML(String file)
	{
		XmlPullParserFactory factory;
		try{
			Log.v(TAG, "Instanziating the XmlPullParser Factory");
			factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			
			Log.v(TAG, "Opening the \"" + file + "\"");
			InputStream in = this.assets.open(file);
			
			Log.v(TAG, "Starting Parsing");
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			return parseXML(parser);
		} catch(XmlPullParserException e){
			Log.e(TAG,"XML Parser has failed:");
			e.printStackTrace();
		} catch(IOException e) {
			Log.e(TAG,"Opening or reading asset " + file + " has failed:");
			e.printStackTrace();
		} finally {
			// Silliest Exception is cause if you uncomment this, if you don't believe me, just try
			// and see for your own self!
			//this.assets.close();
		}
		Log.e(TAG, "Reached unreachable place");
		return null;
	}
	
	private ArrayList<Category> parseXML(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		Log.i(TAG,"parseXML is running");
		ArrayList<Category> categories = new ArrayList<Category>();
		int event = parser.getEventType();
		Category category = null;
		Question question = null;
		String text = "";
		
		while(event != XmlPullParser.END_DOCUMENT){
			String name = parser.getName();
			switch(event){
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				if(name.equalsIgnoreCase("category")){
					category = Category.newInstance(parser.getAttributeValue(0));
				} else if(name.equalsIgnoreCase("query")){
					question = new Question(parser.getAttributeValue(0));
				}
				break;
			case XmlPullParser.TEXT:
				text = parser.getText();
				break;
			case XmlPullParser.END_TAG:
				if(name.equalsIgnoreCase("category")){
					this.question_size += category.size();
					categories.add(category);
				} else if(name.equalsIgnoreCase("query")){
					category.addQuestion(question);
				} else if(name.equalsIgnoreCase("question")){
					question.setQuestion(text);
				} else if(name.equalsIgnoreCase("option")){
					question.addOption(text);
				} else if(name.equalsIgnoreCase("answer")){
					question.addAnswer(text);
				}
				break;
			default:
				Log.e(TAG, "XmlParser ERROR: Unknown Event on line " + parser.getLineNumber());
				break;
			}
			event = parser.next();
		}
		return categories;
	}
	
	public int getQuestionSize()
	{
		return this.question_size;
	}
}
