package com.hanivi.quizapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class MFragment extends Fragment
{	
	public static final String TAG = "MFragment";
	
	private TextView question;
	private RadioGroup opts;
	private Question q;
	
	public static MFragment newInstance(Question question)
	{
		MFragment frag = new MFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("question", question);
		frag.setArguments(bundle);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
	    View view = inflater.inflate(R.layout.multichoice, container, false);
	    int index = 0;
	    this.opts = (RadioGroup) view.findViewById(R.id.opts);
	    this.question = (TextView) view.findViewById(R.id.question_text);
	    this.q = (Question) getArguments().getSerializable("question");
	    
	    this.question.setText(this.q.getQuestion());
		for(String op : this.q.getOptions()) {
			RadioButton rb = new RadioButton(getActivity());
			rb.setText(op);
			rb.setTextSize(24);
			rb.setId(index++);
			//rb.setGravity(Gravity.CENTER);
			this.opts.addView(rb);
		}
	    return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	    this.opts.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				Log.i(TAG, "For " + group + ", clicked on " + checkedId);
				if(q.isCorrect(checkedId)){
					((MainActivity) getActivity()).incScore();
				}
				((Category) getParentFragment()).replaceFrag();
			}
		});
	}
}