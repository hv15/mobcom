package com.hanivi.quizapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DoneFragment extends Fragment {
	
	private TextView done;

	public static DoneFragment newInstance(String category)
	{
		DoneFragment frag = new DoneFragment();
		Bundle bundle = new Bundle();
		bundle.putString("category", category);
		frag.setArguments(bundle);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.done, container, false);
		this.done = (TextView) view.findViewById(R.id.done_text);
		String category = getArguments().getString("category");
		
		this.done.setText("You have completed " + category + "!");
		
		return view;
	}
}