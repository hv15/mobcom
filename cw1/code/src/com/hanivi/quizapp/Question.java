package com.hanivi.quizapp;

import java.io.Serializable;
import java.util.ArrayList;

import com.hanivi.quizapp.inface.MQuestion;
import com.hanivi.quizapp.inface.NQuestion;
import com.hanivi.quizapp.inface.TQuestion;
import com.hanivi.quizapp.inface.YQuestion;

import android.util.Log;

public class Question implements YQuestion, TQuestion, MQuestion, NQuestion, Serializable {
	private static final long serialVersionUID = 4213479662584306026L;
	
	public final String TAG = "Question";
	private String question;
	private String answer;
	
	private ArrayList<String> options;
	private String type;
	private int correctAnswer;
	private String[] answers = {"yes", "no"};
	
	public Question(String type)
	{
		this.type = type;
		if(type.equalsIgnoreCase("m")){
			this.options = new ArrayList<String>(4);
		}
	}

	public String getType()
	{
		return this.type;
	}
	
	public void addAnswer(String a)
	{
		if(this.type.equalsIgnoreCase("y")){
			this.correctAnswer = Integer.parseInt(a);
			this.answer = answers[this.correctAnswer];
		} else if(this.type.equalsIgnoreCase("m")){
			this.correctAnswer = Integer.parseInt(a);
			this.answer = this.options.get(this.correctAnswer);
		} else {
			this.answer = a;
		}
	}
	
	@Override
	public String evalEquation(String[] q)
	{
		int index = 0;
		int out = 0;
		int current = 0;
		
		for(String token : q){
			//Log.i(TAG, token + " at index " + index);
			if(index %2 == 0){
				current = Integer.parseInt(token);
			} else {
				if(token.equals("+")){
					out += current;
				} else if(token.equals("-")){
					out -= current;
				} else if(token.equals("*")){
					out *= current;
				} else if(token.equals("/")){
					out /= current;
				} else {
					Log.e(TAG, "Unknown operator detected: `" + token + "`!");
				}
			}
			index++;
		}
		return String.valueOf(out);
	}
	
	@Override
	public void setQuestion(String q)
	{
		//if(this.type.equalsIgnoreCase("n")){
		//	addAnswer(evalEquation(q.split(" ")));
		//}
		this.question = q;
	}
	
	@Override
	public void addOption(String s, boolean correct) {
		this.options.add(s);
		if(correct){
			this.correctAnswer = options.size();
		}
	}
	
	@Override
	public void addOption(String a)
	{
		this.options.add(a);
	}

	@Override
	public String getAnswer(int i)
	{
		if(this.type.equalsIgnoreCase("mc")){
			return this.options.get(i);
		} else {
			return this.answer;
		}
	}

	@Override
	public String[] getOptions()
	{
		return this.options.toArray(new String[1]);
	}

	@Override
	public String getQuestion()
	{
		return this.question;
	}

	@Override
	public String getAnswer()
	{
		return this.answer;
	}

	@Override
	public boolean isCorrect(String a)
	{
		return this.answer.equalsIgnoreCase(a);
	}

	@Override
	public boolean isCorrect(int o) {
		return this.correctAnswer == o;
	}
}